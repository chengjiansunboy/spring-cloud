/*
 * 深圳市灵智数科有限公司版权所有.
 */
package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能说明
 *
 * @author 程健
 * @version 1.0.0
 * @date 2020/11/30
 */
@SpringBootApplication
public class ZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class,args);
    }
}
