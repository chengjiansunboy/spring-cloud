/*
 * 深圳市灵智数科有限公司版权所有.
 */
package org.example.configuration;

import org.example.rule.DefaultRibbonClientConfiguration;
import org.example.rule.UserProviderRibbonClientConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;

/**
 * 功能说明
 *
 * @author 程健
 * @version 1.0.0
 * @date 2020/11/29
 */
@Configuration
@RibbonClients(value = {
        // 客户端级别的配置
        @RibbonClient(name = "demo-provider", configuration = UserProviderRibbonClientConfiguration.class)
        //默认配置
    }, defaultConfiguration = DefaultRibbonClientConfiguration.class)
public class RobbinConfiguation {


}