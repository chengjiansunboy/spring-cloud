/*
 * 深圳市灵智数科有限公司版权所有.
 */
package org.example.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能说明
 *
 * @author 程健
 * @version 1.0.0
 * @date 2020/11/29
 */
@Configuration
public class DefaultRibbonClientConfiguration {

    @Bean
    public IRule ribbonDefaultRule() {
        return new RoundRobinRule();
    }
}