/*
 * 深圳市灵智数科有限公司版权所有.
 */
package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动入口类
 *
 * @author 程健
 * @version 1.0.0
 * @date 2020/11/29
 */
@SpringBootApplication
public class RibbonApplication {

    public static void main(String[] args) {
        SpringApplication.run(RibbonApplication.class,args);
    }
}